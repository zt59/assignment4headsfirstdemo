package edu.duke.fintech.assignment4headsfirstjsp.web;

import edu.duke.fintech.assignment4headsfirstjsp.model.BeerExpert;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "beerServletV3", value = "/selectbeerv3")
public class BeerSelectV3 extends HttpServlet {
    private String message;

    public void init() {
        message = "Beer Selection Advice";
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String c = request.getParameter("color");
        BeerExpert expert = new BeerExpert();
        List<String> beers = expert.getBrands(c);

        request.setAttribute("beers", beers);
        RequestDispatcher view = request.getRequestDispatcher("result.jsp");
        view.forward(request,response);
    }

    public void destroy() {
    }
}